let imgSelect = document.getElementById("img-upload");
let imgPreview = document.getElementById("img-preview");
var uploadCaption = document.getElementById("upload-caption");
let model = undefined;
let predResult = document.getElementById("result");

imgSelect.addEventListener("change", fileSelectHandler, false);

function fileSelectHandler(e) {
  var files = e.target.files || e.dataTransfer.files;
  for (var i = 0, f; (f = files[i]); i++) {
    previewImg(f);
  }
}

async function initialize() {
  model = await tf.loadLayersModel("../weights/model.json");
}

async function predict() {
  let tensorImg = tf.browser
    .fromPixels(imgPreview)
    .resizeNearestNeighbor([150, 150])
    .toFloat()
    .expandDims();
  prediction = await model.predict(tensorImg).data();
  if (prediction[0] === 0) {
    predResult.innerHTML = "It is a cat";
  } else if (prediction[0] === 1) {
    predResult.innerHTML = "It is a dog";
  } else {
    predResult.innerHTML = "It is neither";
  }
  show(predResult);
}

function reset() {
  imgSelect.value = "";
  imgPreview.src = "";
  predResult.innerHTML = "";
  hide(imgPreview);
  hide(predResult);
  show(uploadCaption);
}

function previewImg(file) {
  var reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onloadend = () => {
    imgPreview.src = URL.createObjectURL(file);
    show(imgPreview);
    hide(uploadCaption);
  };
}
function hide(el) {
  el.classList.add("hidden");
}

function show(el) {
  el.classList.remove("hidden");
}

initialize();
